# Coursework about fluctuations

This is a term paper created entirely for Ruslan. In it a model of oscillations is created, which is a solution of the wave equation

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install -e .
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)